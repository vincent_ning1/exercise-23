﻿using System;
using System.Collections.Generic;

namespace exercise_23
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            var  dict = new Dictionary<string,string>();
            dict.Add("Deadpool","Comedy");
            dict.Add("The Jungle Book","Adventure");
            dict.Add("Titanic","Romantic");
            dict.Add("Pirates of the Caribbean","Comedy");
            dict.Add("Fifty shades dark","Romantic");
            foreach (var movie in dict)
            {
               Console.WriteLine($"{movie.Key}---{movie.Value}");
            }
            Console.ReadKey();
            Console.Clear();


           var cntComedy = 0;
           var cntAction = 0;
           var cntDrama = 0;
           foreach(var movie in dict)
           {
               if(movie.Value == "Comedy")
               {
                    ++cntComedy;
                    
               }
               else if(movie.Value == "Action")
               {
                    ++cntAction;
               }
               else if(movie.Value == "Drama")
               {
                    ++cntDrama;
               }
           }
           Console.WriteLine($"Comedy              :{cntComedy}");
           Console.WriteLine($"Action              :{cntAction}");
           Console.WriteLine($"Drama               :{cntDrama}");
           Console.ReadKey();
           Console.Clear();
           
           Console.WriteLine("Comedy:");
           foreach(var movie in dict)
           
             if (movie.Value == "Comedy")
            
              {     
                   Console.WriteLine($"{movie.Key}");
              } 
              Console.WriteLine();Console.WriteLine();Console.WriteLine();
                Console.WriteLine("Action:");
          foreach(var movie in dict)         
             if (movie.Value == "Action")
              { 
                 
                 Console.WriteLine($"{movie.Key}");
              }
              Console.WriteLine();Console.WriteLine();Console.WriteLine();
              Console.WriteLine("Drama:");
          foreach(var movie in dict)
                
             if (movie.Value == "Drama")
              { 
                 
                 Console.WriteLine($"{movie.Key}");
              }
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
